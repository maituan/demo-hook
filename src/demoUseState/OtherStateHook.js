import React, { useState } from 'react'

function ObjectStateHook() {

    const [name, setName] = useState({ firstName: '', lasttName: '' })

    return (
        <form>
            <input type='text'
                value={name.firstName}
                placeholder='first name'
                onChange={(e) => setName({ ...name, firstName: e.target.value })} />
            <input type='text'
                value={name.lasttName}
                placeholder='last name'
                onChange={(e) => setName({ ...name, lasttName: e.target.value })} />
            <h2>First Name - {name.firstName}</h2>
            <h2>Last Name - {name.lasttName}</h2>
        </form>
    )
}

export default ObjectStateHook
