import React, { Component } from 'react'

export default class PrevStateCounterClass extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }

    increasingCount = () => {
        let self = this
        for (let index = 0; index < 5; index++) {
            this.setState(prevState => {
                return {
                    count: prevState.count + 1
                }
            })
            
        }
    }
    render() {
        return (
            <div>
                <button onClick={this.increasingCount}>Count {this.state.count}</button>
            </div>
        )
    }
}
