import React, { useState } from 'react'

function HookCounter() {

    const [count, setCount] = useState(0)

    const increasingCount = () => {
        setCount(count + 1)
    }

    return (
        <div>
            <button onClick={increasingCount}>Count {count}</button>
        </div>
    )
}

export default HookCounter
