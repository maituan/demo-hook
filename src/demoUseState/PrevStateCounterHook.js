import React, { useState } from 'react'

function PrevStateCounterHook() {
    const initialCount = 0
    const [count, setCount] = useState(initialCount)

    const increasingCount = () => {
        for (let index = 0; index < 5; index++) {
            setCount(prevCount => prevCount + 1)
            console.log('here')
        }
    }

    return (
        <div>
            Count: {count}
            <button onClick={() => setCount(initialCount)}>Reset</button>
            <button onClick={() => setCount(count + 1)}>Increase</button>
            <button onClick={() => setCount(count - 1)}>Decrease</button>
            <button onClick={increasingCount}>Increase Five</button>
        </div>
    )
}

export default PrevStateCounterHook
