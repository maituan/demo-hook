import React from 'react'
import ClassCounter from './demoUseState/ClassCounter'
import HookCounter from './demoUseState/HookCounter'
import PrevStateCounterHook from './demoUseState/PrevStateCounterHook'
import PrevStateCounterClass from './demoUseState/PrevStateCounterClass'
import ObjectStateHook from './demoUseState/OtherStateHook'
import ClassDocumentTitle from './demoUseEffect/ClassDocumentTitle'
import HookDocumentTitle from './demoUseEffect/HookDocumentTitle'
import CleanUpUseEffect from './demoUseEffect/CleanUpUseEffect'
import MouseContainer from './demoUseEffect/MouseContainer'
import FetchingHook from './demoUseEffect/FetchingHook'
import ComponentA from './demoUseContext/ComponentA'
import CountExample from './demoUseReducer/CountExample'
import Parent from './demoUseCallback/Parent'
import CountUseMemo from './demoUseMemo/CountUseMemo'
import Input from './demoUseRef/Input'
import HookTimer from './demoUseRef/HookTimer'
import DocTitleOne from './demoCustomHook/DocTitleOne'
import CounterOne from './demoCustomHook/CounterOne'
import IncorrectDependency from './demoUseEffect/IncorrectDependency'
// export const UserContext = React.createContext()
// export const PodContext = React.createContext()
function App() {
  return (
    <div style={{ textAlign: 'center' }} className="App">
     Welcome to React Hook - Quéo Com Tu Rì Ắk Hút (vietnamese)
    </div>
  );
}

export default App;
