import React, { useState } from 'react'

function CounterTwo() {
    const [count, setCount] = useState(0)

    const increasingCount = () => {
        setCount(prevCount => prevCount + 1)
    }

    const decreaseCount = () => {
        setCount(prevCount => prevCount - 1)
    }

    const reset = () => {
        setCount(0)
    }



    return (
        <div>
            Count: {count}
            <button onClick={increasingCount}>Increase</button>
            <button onClick={decreaseCount}>Decrease</button>
            <button onClick={reset}>Reset</button>
        </div>
    )
}

export default CounterTwo
