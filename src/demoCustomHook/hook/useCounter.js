import { useState } from 'react'

function useCounter(initialCount) {
    const [count, setCount] = useState(initialCount)

    const increasingCount = () => {
        setCount(prevCount => prevCount + 1)
    }

    const decreaseCount = () => {
        setCount(prevCount => prevCount - 1)
    }

    const reset = () => {
        setCount(0)
    }
    return [count, increasingCount, decreaseCount, reset]
}

export default useCounter
