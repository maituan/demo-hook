import React, { useEffect, useState } from 'react'

function HookDocumentTitle() {
    const [count, setCount] = useState(0)
    const [value, setValue] = useState('')

    useEffect(() => {
        console.log('Update document title')
        document.title = `Clicked ${count} time(s)`
    }, [count])

    return (
        <div>
            <button onClick={() => setCount(count + 1)}>Count {count}</button>
            <input value={value} onChange={(e) => setValue(e.target.value)}/>
        </div>
    )
}

export default HookDocumentTitle
