import React, { useState } from 'react'
import CleanUpUseEffect from './CleanUpUseEffect'

function MouseContainer() {
    const [display, setDisplay] = useState(true)
    return (
        <div>
            <button onClick={() => setDisplay(!display)}>Toggle Display MouseLog</button>
            {display && <CleanUpUseEffect/>}
        </div>
    )
}

export default MouseContainer
