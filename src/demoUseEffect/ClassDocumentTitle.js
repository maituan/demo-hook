import React, { Component } from 'react'

export default class ClassDocumentTitle extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }

    componentDidMount() {
        document.title = `Clicked ${this.state.count} time(s)`
    }

    componentDidUpdate() {
        document.title = `Clicked ${this.state.count} time(s)`
    }

    render() {
        return (
            <div>
                <button onClick={() => {this.setState({count: this.state.count + 1})}}>Count {this.state.count}</button>
            </div>
        )
    }
}
