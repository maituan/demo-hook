import React, {useState, useEffect} from 'react'

function CleanUpUseEffect() {
    const [x, setX] = useState(0)
    const [y, setY] = useState(0)

    const logMousePosition = e => { 
        console.log('Inside logMousePosition')
        setX(e.clientX)
        setY(e.clientY)
    }

    useEffect(() => {
        console.log('useEffect called')

        window.addEventListener('mousemove', logMousePosition)
        // return () => {
        //     cleanup
        // }
    }, [])

    return (
        <div>
            Hook Mouse {x} - {y}
        </div>
    )
}

export default CleanUpUseEffect
