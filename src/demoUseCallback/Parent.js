import React, { useState } from 'react'
import Title from './Title'
import Count from './Count'
import Button from './Button'

function Parent() {
    const [age, setAge] = useState(27)
    const [salary, setSalary] = useState(10000)
    

    const increaseAge = () => {
        setAge(age + 1)
    }

    const increaseSalary = () => {
        setSalary(salary + 1000)
    }

    return (
        <div>
            <Title />
            <Count text='Age' count={age} />
            <Button handleClick={increaseAge}>Increase Age</Button>
            <Count text='Salary' count={salary} />
            <Button handleClick={increaseSalary}>Increase Salary</Button>
        </div>
        )
    }
    
    export default Parent
