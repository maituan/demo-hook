import React, { useReducer } from 'react'

const initialState = 0

const reducer = (state, action) => {
    switch (action) {
        case 'increasement':
            return state + 1
        case 'decreasement':
            return state - 1
        case 'reset':
            return initialState
        default:
            return state
    }
}

function CountSimple() {

    const [count, dispatch] = useReducer(reducer, initialState)

    return (
        <div>
            Count1 : {count}
            <hr/>
            <button onClick={() => dispatch('increasement')}>Increase Counter</button>
            <button onClick={() => dispatch('decreasement')}>Decrease Counter</button>
            <button onClick={() => dispatch('reset')}>Reset</button>
        </div>
    )
}

export default CountSimple
