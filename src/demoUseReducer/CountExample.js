import React, { useReducer } from 'react'

const initialState = {
    firstCounter: 0,
    secondCounter: 10
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'increasement1':
            return {...state, firstCounter: state.firstCounter + action.value}
        case 'decreasement1':
            return {...state, firstCounter: state.firstCounter - action.value}
        case 'increasement2':
            return {...state, secondCounter: state.secondCounter + action.value}
        case 'decreasement2':
            return {...state, secondCounter: state.secondCounter - action.value}
        case 'reset':
            return initialState
        default:
            return state
    }
}

function CountExample() {

    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <div>
            Count1 : {state.firstCounter} <span>     Count2: {state.secondCounter}</span>
            <hr/>
            <button onClick={() => dispatch({ type: 'increasement1', value: 1 })}>Increase Counter 1</button>
            <button onClick={() => dispatch({ type: 'decreasement1', value: 1 })}>Decrease Counter 1</button>
            <hr />
            <button onClick={() => dispatch({ type: 'increasement2', value: 2 })}>Increase Counter 2</button>
            <button onClick={() => dispatch({ type: 'decreasement2', value: 2 })}>Decrease Counter 2</button>
            <hr/>
            <button onClick={() => dispatch({ type: 'reset' })}>Reset Counter 1</button>
        </div>
    )
}

export default CountExample
