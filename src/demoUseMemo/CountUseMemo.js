import React, { useState, useMemo } from 'react'

function CountUseMemo() {
    const [countOne, setCountOne] = useState(0)
    const [countTwo, setCountTwo] = useState(0)

    const increaseOne = () => {
        setCountOne(countOne + 1)
    }

    const increaseTwo = () => {
        setCountTwo(countTwo + 1)
    }

    const isEven = () => {
        let i = 0
        while( i < 10) {
            console.log(`Inside While`)
            i++
        }
        return countOne % 2 === 0
    }

    return (
        <div>
            <button onClick={increaseOne}>Count One - {countOne}</button>
            <div>{isEven() ? 'Even' : 'Odd'}</div>
            <button onClick={increaseTwo}>Count Two - {countTwo}</button>
        </div>
    )
}

export default CountUseMemo
